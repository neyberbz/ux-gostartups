import axios from 'axios';

const BASE_URL = 'http://ws.conectagro.com/';

export { getPublicProduct, getPublicCategory };

function getPublicProduct() {
    const url = `${BASE_URL}api/productos`;
    return axios.get(url).then(response => response.data);
}

function getPublicCategory() {
    const url = `${BASE_URL}api/categorias`;
    return axios.get(url).then(response => response.data);
}
