import Home from './components/Home.vue'
import Noticia from './components/Noticia.vue'
import Categoria from './components/Categoria.vue'

export const routes = [
    {path: '/', component: Home},
    {path: '/:slug', component: Noticia},
    {path: '/seccion/:slug', component: Categoria}
]
